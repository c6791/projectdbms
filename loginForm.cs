﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace loginModel
    
{
    
    public partial class loginForm : Form
    {
        public Boolean test=false;
        public loginForm()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Button btn = sender as Button;
            //btn.Text = "";
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection cnn = new SqlConnection();
                cnn = connectToDatabase.getConnection(usernameLogin.Text, passwordLogin.Text);
                
                //mở database đã chọn để thao tác
                cnn.Open();
                //thông báo nếu đã kết nối thành công
                MessageBox.Show("Login success!", "Notification", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.OK;
                test = true;
                //đóng đối tượng chứa kết nối sau khi kết nối thành công
                cnn.Close();
            } catch (Exception)
            {
                test = false;
                MessageBox.Show("Username or password is not correct!", "Notification", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }

        private void passwordLogin_TextChanged(object sender, EventArgs e)
        {
            passwordLogin.PasswordChar = '*';
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace loginModel
{
    public partial class staffForm : Form
    {
        public staffForm()
        {
            InitializeComponent();
        }

        private void staffForm_Load(object sender, EventArgs e)
        {
            List<Staff> staffList = new List<Staff>();
            SqlConnection conn = new SqlConnection();
            conn = connectToDatabase.getConnection(connectToDatabase.user, connectToDatabase.password);

            conn.Open();
            SqlCommand sqlCmd = new SqlCommand("select Staff_ID, Staff_Name, Position, Hometown, Gender, Image from Staff", conn);
            SqlDataReader dataReader = sqlCmd.ExecuteReader();
            while (dataReader.Read())
            {
                Staff staff = new Staff((String)dataReader["Staff_ID"], (String)dataReader["Staff_Name"], (String)dataReader["Position"], (String)dataReader["Hometown"], (String)dataReader["Gender"], (byte[])dataReader["Image"]);
                staffList.Add(staff);
            }
            conn.Close();

            staffTable.DataSource = staffList;
            staffTable.ReadOnly = true;
            staffTable.RowTemplate.Height = 50;
            staffTable.AllowUserToAddRows = false;

            DataGridViewImageColumn imgCol = new DataGridViewImageColumn();
            imgCol = (DataGridViewImageColumn)staffTable.Columns[5];
            imgCol.ImageLayout = DataGridViewImageCellLayout.Stretch;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void addNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addStaffForm addNewStudentForm = new addStaffForm();
            addNewStudentForm.Show(this);
        }

    }
}
